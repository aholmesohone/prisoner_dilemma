namespace PrisonerDilemma;
//when you spend alot of time watching youtube, you'll learn about the prisoners dilemma weather you like it or not.
//including a video about this exact kind of competition.
class Prisoner2042141 : IPrisoner
{
  public string Name { get; } = "2042141";
  private PrisonerChoice opponentPreviousChoice {get; set;} = PrisonerChoice.Cooperate;
  public void Reset(DilemmaParameters dilemmaParameters) 
  {
    opponentPreviousChoice = PrisonerChoice.Cooperate;
  }

  public PrisonerChoice GetChoice()
  {

    return opponentPreviousChoice;
  }

  public void ReceiveOtherChoice(PrisonerChoice otherChoice) 
  { 
    opponentPreviousChoice = otherChoice;
  }
}